#!/usr/bin/ruby

require 'pathname'
require 'iniparser'
require 'csv'

ini_pathname = Pathname(ARGV[0])
#csv_pathname = Pathname(ARGV[1]) if ARGV[1]

ini_data = INI.load(ini_pathname.read)

output_data = ["title = #{ini_data['title']}\n"]

ini_data
  .reject { |k, v| k == 'title' }
  .sort_by { |k, v| k }
  .each do |k, v|
    output_data.push(
      <<~EOS
        [#{k}]
          title = #{v['title']}
          link  = #{v['link']}
          feed  = #{v['feed']}
      EOS
    )
  end

ini_pathname.write(output_data.join("\n"))
