# TODO

## Sites which do not have feeds

Sites which do not have feeds which we can read, figure how to scrap them in the future.

### Articles

* [CBC Kitchener-Waterloo](http://www.cbc.ca/news/canada/kitchener-waterloo), [feed](https://www.cbc.ca/cmlink/rss-canada-kitchenerwaterloo)
* [Kitchener Citizen](http://www.kitchenercitizen.com)
* [Snapd - Kitchener-Waterloo](https://kitchenerwaterloo.snapd.com/)
* [Inside the Perimeter](https://insidetheperimeter.ca/)
* [Good Company Magazine](http://goodcomagazine.com/)
* [Aashni Shah](http://blog.aashni.me/)
* [Betakit Waterloo](https://betakit.com/tag/waterloo/)
* [Joseph Fung](http://www.josephfung.ca/)
* [Culture Fancier](http://www.culturefancier.com/)
* [Joe Kidwell](http://joekidwell.net/)
* [Charlie Drage](http://charliedrage.com/blog/)
* [Region of Waterloo New and Public Notices](https://www.regionofwaterloo.ca/Modules/News/search.aspx)
* [University of Waterloo Magazine](https://uwaterloo.ca/magazine/)
* [Jen Vasic](https://jenvasicwaterloo.ca/)
  - site does have RSS but it is empty
* [Chelsea Healthy Kitchen](http://chelseashealthykitchen.com/)
* [Emmett MacFarlane](http://www.emmettmacfarlane.com/)
* [Make it Kitchener](http://www.makeitkitchener.ca/)
* [Simon Pratt](http://blog.pr4tt.com/)
* [Kitchener Waterloo Community Foundation](https://www.kwcf.ca/)
* [Waterloo Region Greens](https://wrgreens.wordpress.com/)
* [Accelerator Centre](http://acceleratorcentre.com/)
* [Onion Honey](https://onionhoney.com/news--2)
* [Danny Michel](https://www.dannymichel.com/)
* [ENDURrun](https://www.endurrun.com/news/)
* [Belmont Village](http://thebelmontvillage.ca/news/)
* [Timeless Cafe and Bakery](http://www.timelesscafeandbakery.com/)
* [Diane Vernel's MPP Blog](https://us10.campaign-archive.com/home/?u=28b7d772a5bd2e30960f3004f&id=eec263041e)
* [KW Legacy](http://www.kwlegacy.ca/theprogram.php)
* [Cbrige.ca](https://cbridge.ca/)
* [Waterloo Stories](https://uwaterloo.ca/stories/)
* [Cooking with Linux](http://www.cookingwithlinux.com/)
* [Paulo Moncores](http://paulomoncores.com/)
* [The Cord](https://thecord.ca/)
* [CycleWR](http://cyclewr.ca/)
* [Lazaridis Institue](https://lazaridisinstitute.wlu.ca/news/index.html)
* [Tenille Bonogure](https://www.tenilleb.com/news)
* [Bob Jonkman](https://bobjonkman.ca/)
* [Jason Panda](http://kwpanda.com/)
* [Suzanne Church](http://suzannechurch.com/wordpress/)
* [Sarah Tolmie](http://sarahtolmie.ca/)
* [Grobo](https://www.grobo.io/pages/blog)
* [SydFit Health](https://sydfithealth.ca/blog-2/)
* [Connolly Design](http://www.connollydesign.com/)
* [Five Hundred Sparks](http://www.fivehundredsparks.ca/)
* [Waterloo County Rugby Club](https://www.waterloocountyrugby.com/news)
* [Waterloo Global Science Initiative(WGSI)](http://www.wgsi.org/blog)
* [Deutschmann Law](https://www.deutschmannlaw.com/blog/archive)
* [Prabhakar Ragde ](https://cs.uwaterloo.ca/~plragde/)
* [It's Me, Melis](https://www.itsmemelis.com/blog)

### Audio

* [Centre for International Governance Innovation](https://www.cigionline.org/multimedia#block-views-block-videos-block-5)
* https://soundcloud.com/todddonald

### Video

* [Centre for International Governance Innovation](https://www.cigionline.org/multimedia#block-views-block-videos-block-4)
* [Perimiter Intitue video archive](http://pirsa.org/)

### Images
* https://www.flickr.com/photos/matthewsmithphoto
* https://www.instagram.com/matt.skw/
* https://www.matthewsmithphoto.net/
* https://www.instagram.com/richard_garvey123/
* https://www.instagram.com/starlightsocialclub/
* http://www.instagram.com/onionhoneymusic/
* https://www.instagram.com/jojoworthington/
* https://www.instagram.com/craftoberfestkw/
* https://www.instagram.com/theguitarcorner/
* https://www.instagram.com/conestogaconnected/
* https://www.instagram.com/robinmazumder/

## Repositories
* https://git.sr.ht/%7Esingpolyma/
* https://bitbucket.org/albertoconnor/

## Unsorted Data and notes about data to find

* find the feeds for all the schools, universities and colleges in the region
* find sunshine list entries for the region
  - https://docs.google.com/document/d/1kSTnKfStYV5QW6noFR-G_5hxAz4J7GnUJhTjT4UA03A/edit
  - https://www.reddit.com/r/kitchener/comments/6x8ct2/city_of_kitchener_2016_sunshine_list_rankings_and/
* find statscan data which covers the region
* find data/feeds for AR Games
  - PokemonGo
  - Wizard's Unite
  - Ingress
* collect lists of march break/christmas/summer camps around the region
  - https://www.reddit.com/r/kitchener/comments/820ewm/march_break_day_camps/
  - https://horsebackadventures.ca/march-break-camp/
  - funworks
  - ymca
  - humane society
* https://juliewitmermaps.com/

## ICEBOX Unsorted project ideas

If they become more solid it might be worth considering making these into
issues.

* add a basic leaflet map, which is scoped to Waterloo Region
  - then can start adding things to it
  - investigate adding a timeline
    * https://github.com/hallahan/LeafletPlayback
  - sidebar
    * https://github.com/Turbo87/sidebar-v2
  - try to re-use the plugins which are used by OSM already
* add re-direct files for the short domains
* should add license notices
  - AGPLv3 for code
  - CC-BY-SA4.0 for documentation
  - ODbL for data?
    * https://wiki.openstreetmap.org/wiki/Open_Database_License
  - research
    * https://wiki.openmod-initiative.org/wiki/Choosing_a_license
    * https://en.wikipedia.org/wiki/Wikipedia:FAQ/Copyright#Can_I_add_something_to_Wikipedia_that_I_got_from_somewhere_else.3F
    * apparently CC-BY-SA4.0 is not compatiable with wikiepdia
      - https://meta.wikimedia.org/wiki/Terms_of_use/Creative_Commons_4.0/Legal_note
      - but they seem to be upgrading to CC-BY-SA4.0
    * https://meta.wikimedia.org/wiki/Terms_of_use/Creative_Commons_4.0/FAQ
    * https://en.wikipedia.org/wiki/Wikipedia:Copying_text_from_other_sources
    * https://en.wikipedia.org/wiki/Wikipedia:Copyrights
* add a CONTRIBUTORS file
* get HTTPS working
  - https://mkkhedawat.github.io/Enabling-HTTPS-for-Gitlab-pages-using-Certbot/
  - https://about.gitlab.com/2016/04/11/tutorial-securing-your-gitlab-pages-with-tls-and-letsencrypt/
  - https://autonomic.guru/lets-encrypt-gitlab-again/
  - https://swas.io/blog/automatic-letsencrypt-gitlab-pages/
  - https://arothuis.nl/posts/lets-encrypt-gitlab-pages/
    * this one looks the most automated
    * using gitlab-le
  - https://mkkhedawat.github.io/Enabling-HTTPS-for-Gitlab-pages-using-Certbot/
  - https://github.com/rolodato/gitlab-letsencrypt
* consider writing a middleman-pluto extension
* document some uses cases
  - what restaurant is opening here?
    * https://twitter.com/m2bowman/status/1163928812251484160
  - pull all of uses cases from my presentation
* review the OpenStreetMap wiki and link Waterloo related things
  - https://wiki.openstreetmap.org/wiki/Waterloo_region
  - https://wiki.openstreetmap.org/wiki/Kitchener-Waterloo_mapping_party
  - https://www.bbbike.org/Waterloo/
  - https://wiki.openstreetmap.org/wiki/Kitchener-Waterloo
  - https://wiki.openstreetmap.org/wiki/Kitchener-Waterloo/Import
* wikipedia pages
  - https://en.wikipedia.org/wiki/James_Alan_Gardner
* create a list of use cases
  - bring in the examples that I used in my presentation
  - https://twitter.com/m2bowman/status/1163928812251484160
  - https://twitter.com/m2bowman/status/1168492401389948928
* research sources of unionization data
